## meta-plambs-demo
-------------------

### Description
This is a personnal Yocto layers i use to experiment with Yocto.

### Dependencies
This layer depend on the following layers:
- poky
- meta-openembedded

### Build
The easiest way to build a recipe from this layers is to use my yocto template [here](https://gitlab.com/plambs/yocto)
