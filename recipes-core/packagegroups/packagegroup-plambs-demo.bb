SUMMARY = "plambs package group"
DESCRIPTION = "plambs custom package group included in core-image-plambs-demo"
LICENSE = "MIT"

inherit packagegroup

PACKAGES = "\
	packagegroup-plambs-demo-system-apps \
"

RDEPENDS_packagegroup-plambs-demo-system-apps = "\
	psplash \
	wayland \
	weston \
	dropbear \
	lvgl-demo \
"
