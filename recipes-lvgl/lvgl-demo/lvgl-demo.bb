SUMMARY = "lvgl demo on linux"
DESCRIPTION = "lvgl demo on linux using sdl"
LICENSE = "MIT"

LIC_FILES_CHKSUM = "file://licence.txt;md5=63f3630b5e5f79016a79b2e7797fc31d"

SRCREV = "ff75519f79021ee50a235ff99049131e48b4f1c5"
SRCBRANCH = "master"
SRC_URI = "gitsm://github.com/lvgl/lv_sim_eclipse_sdl.git;branch=master;protocol=https"

DEPENDS += " libsdl2 lvgl"
RDEPENDS_${PN} += " libsdl2 lvgl"

S = "${WORKDIR}/git"

inherit cmake

EXTRA_OECMAKE = ""

do_install() {
    install -m 0755 -D ${WORKDIR}/git/bin/main ${D}/usr/bin/lvgl-demo
}
